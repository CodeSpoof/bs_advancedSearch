#!/usr/bin/python3
import json
import os
import sqlite3
import threading
import time
from os.path import isfile
from sqlite3 import Cursor

from bs4 import BeautifulSoup
import requests
from threading import Lock
from urllib.parse import urljoin


def generator() -> tuple[list, str]:
    class GenreFetcher(threading.Thread):
        def __init__(self, _url):
            super().__init__()
            self.result = None
            self.url = _url

        def run(self) -> None:
            tags = []
            html = BeautifulSoup(requests.get(self.url).text, features="html.parser")
            print(html.find("title").text)
            for elem in html.find("div", {"class": "infos"}).find("div").find("p").find_all("span"):
                tags.append(elem.text)
            self.result = tags

    genres = []
    lis_ = []
    for element_ in BeautifulSoup(requests.get("https://bs.to/andere-serien").text, features="html.parser").find_all("div", {"class": "genre"}):
        for show_ in element_.find_all("li"):
            while threading.active_count() > 100:
                time.sleep(0.1)
            lis_.append(GenreFetcher(urljoin("https://bs.to/andere-serien", show_.find("a")["href"])))
            lis_[len(lis_) - 1].start()
    while threading.active_count() > 1:
        time.sleep(0.1)
    for fetcher in lis_:
        genres = list(set(genres) | set(fetcher.result))
    ids = []
    genres.sort()
    for genre in genres:
        ids.append("genre_" + genre.lower().replace("-", "_").replace(" ", "_"))
    return genres, "CREATE TABLE shows (id text, title text, desc text, " + " INTEGER DEFAULT 0, ".join(ids) + " INTEGER DEFAULT 0);"


def add_show(id, title, desc, genres):
    temp = []
    for genre in genres:
        if genre in all_genres:
            temp.append("genre_" + genre.lower().replace("-", "_").replace(" ", "_"))
    ids = ", ".join(temp)
    ones = ", ".join(["1" for genre in genres])
    with sqlite_lock:
        sql = f"INSERT INTO shows(id, title, desc, {ids}) VALUES ('{id}', '{title}', '{desc}', {ones})"
        print(title)
        cur.execute(f"INSERT INTO shows(id, title, desc, {ids}) VALUES ('{id}', ?, ?, {ones})", [title, desc])


class Fetcher(threading.Thread):
    def __init__(self, _url):
        super().__init__()
        self.url = _url

    def run(self) -> None:
        tags = []
        html = BeautifulSoup(requests.get(self.url).text, features="html.parser")
        for elem in html.find("div", {"class": "infos"}).find("div").find("p").find_all("span"):
            tags.append(elem.text)
        for ej in html.select("#seasons a"):
            htx = BeautifulSoup(requests.get("https://bs.to/" + ej['href']).text, features="html.parser")
            num1_ = 0 if ej.text == "Specials" else int(ej.text)
            for el in htx.find("table", {"class": "episodes"}).find_all("tr"):
                title_ = el.find("td").find("a")['title']
                num2_ = int(el.find("td").find("a").text)
                html_ = BeautifulSoup(requests.get("https://bs.to/" + el.find("td").find("a")['href']).text, features="html.parser")
                desc_ = html_.find("p", {"id": "spoiler"}).text
                with sqlite_lock:
                    cur.execute(f"INSERT INTO episodes(show_id, season_number, episode_number, title, desc) VALUES (?, ?, ?, ?, ?)", [self.url, num1_, num2_, title_, desc_])
        title = html.find('h2').text.replace('\t', '').strip("\t\n ").split("\n")[0]
        desc = html.find("div", {"id": "sp_left"}).find("p").text
        add_show(self.url, title.replace("'", "''"), desc.replace("'", "''"), tags)
        lis.remove(self)
        with sqlite_lock:
            con.commit()


if __name__ == '__main__':
    try:
        os.remove("bs.db")
        os.remove("bs.db-journal")
    except:
        pass
    sqlite_lock = Lock()
    genre_lock = Lock()
    all_genres = ""
    shows_sql = ""
    if isfile("genres.json"):
        all_genres = json.load(open("genres.json", "r"))
        ids = []
        for genre in all_genres:
            ids.append("genre_" + genre.lower().replace("-", "_").replace(" ", "_"))
        shows_sql = "CREATE TABLE shows (id text, title text, desc text, " + " INTEGER DEFAULT 0, ".join(ids) + " INTEGER DEFAULT 0);"
    else:
        all_genres, shows_sql = generator()
        json.dump(all_genres, open("genres.json", "w+"), indent=4)
    con = sqlite3.connect('bs.db', check_same_thread=False)
    cur = con.cursor()
    cur.execute(shows_sql)
    cur.execute("CREATE TABLE episodes (show_id text, season_number INTEGER, episode_number INTEGER, title text, desc text)")
    lis = []
    for element in BeautifulSoup(requests.get("https://bs.to/andere-serien").text, features="html.parser").find_all(
            "div", {"class": "genre"}):
        for show in element.find_all("li"):
            while len(lis) > 100:
                time.sleep(0.1)
            lis.append(Fetcher(urljoin("https://bs.to/andere-serien", show.find("a")["href"])))
            lis[len(lis) - 1].start()
    while len(lis) > 1:
        time.sleep(0.1)
    con.commit()
