import datetime
import json
import os
import sqlite3
import threading
import time
from os.path import isfile
from threading import Lock

import dns.resolver
import qdarkgraystyle as qdarkgraystyle
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import QThread
from urllib3.util import connection

from gui import Ui_MainWindow
from parsing import searchTermParser


_dns_providers = {
        "cloudflare": [
            "1.1.1.1",
            "1.0.0.1",
        ],
        "google": [
            "8.8.8.8",
            "8.8.4.4", ],
        "censurfridns": [
            "89.233.43.71",
            "130.225.244.166",
            "130.226.161.34",
            "185.38.27.139",
            "198.180.150.12",
        ],
        "dns.watch": [
            "84.200.69.80",
            "84.200.70.40",
        ],
        "opendns": [
            "208.67.222.222",
            "208.67.220.220",
            "208.67.222.220",
            "208.67.220.222",
        ],
        "opennic": [
            "194.36.144.87",
        ],
        "freedns": [
            "37.235.1.177",
            "172.104.237.57",
            "172.104.49.100",
            "45.33.97.5",
        ],
        "yandex.dns": [
            "77.88.8.8",
            "77.88.8.1",
        ],
        "safeserve": [
            "198.54.117.10",
            "198.54.117.11",
        ],
        "safe_dns": [
            "195.46.39.39",
            "195.46.39.40",
        ],
    }
_orig_create_connection = connection.create_connection


def myResolver(_host, dnssrv, timeout = 5.0):
    r = dns.resolver.Resolver()
    r.nameservers = dnssrv
    r.lifetime = timeout
    answers = r.resolve(_host)
    for rdata in answers:
        return str(rdata)


def patched_create_connection(address, *args, **kwargs):
    """Wrap urllib3's create_connection to resolve the name elsewhere"""
    # resolve hostname to an ip address; use your own
    # resolver here, as otherwise the system resolver will be used.
    _host, _port = address
    _hostname = myResolver(_host, _dns_providers["cloudflare"])

    return _orig_create_connection((_hostname, _port), *args, **kwargs)


#connection.create_connection = patched_create_connection


def def_toplevel(val0, val1, val2):
    lil = [
        "Loading Tags...",
        "Waiting for threads to finish (Tags)...",
        "Creating final list...",
        "Loading DB contents...",
        "Waiting for threads to finish (Contents)...",
    ]
    ui.labelTopLevelStep.setText(lil[val0])
    ui.progressTopLevelStep.setMaximum(val2)
    ui.progressTopLevelStep.setValue(val1)


def def_genre(val0, val1):
    ui.progressGenres.setMaximum(val1)
    ui.progressGenres.setValue(val0)


def def_show(val0, val1):
    ui.progressShows.setMaximum(val1)
    ui.progressShows.setValue(val0)


class Refresher(QThread):
    sig_toplevel = QtCore.pyqtSignal([int, int, int])
    sig_genre = QtCore.pyqtSignal([int, int])
    sig_show = QtCore.pyqtSignal([int, int])
    sig_fin = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()
        ui.stackedWidget.setCurrentIndex(1)
        self.sig_toplevel.connect(def_toplevel)
        self.sig_genre.connect(def_genre)
        self.sig_show.connect(def_show)
        self.sig_fin.connect(self.fin)
        self.start()

    def fin(self):
        ui.stackedWidget.setCurrentIndex(0)

    def emit(self, sig: QtCore.pyqtSignal, curr, maxi):
        sig.emit(curr, maxi)

    def emit_toplevel(self, status, curr=0, maxi=0):
        self.sig_toplevel.emit(status, curr, maxi)

    def run(self) -> None:
        from bs4 import BeautifulSoup
        import requests
        from urllib.parse import urljoin
        self.emit_toplevel(0)

        def generator() -> tuple[list, str]:
            generator_results = []
            generator_lock = Lock()
            generator_primary_lock = Lock()

            class GenreFetcher(QThread):
                def __init__(self, _url):
                    super().__init__()
                    self.url = _url

                def run(self) -> None:
                    tags = []
                    html = BeautifulSoup(requests.get(self.url).text, features="html.parser")
                    for elem in html.find("div", {"class": "infos"}).find("div").find("p").find_all("span"):
                        tags.append(elem.text)
                    with generator_lock:
                        generator_results.append(tags)
                    with generator_primary_lock:
                        lis_.remove(self)
            if isfile("../genres.json"):
                ids = []
                genres = json.load(open("../genres.json", "r"))
                genres.sort()
                for genre in genres:
                    ids.append("genre_" + genre.lower().replace("-", "_").replace(" ", "_"))
                # TODO switch language for optimized approach (like with genres)
                # TODO get rid of Tag-Loader (generator) by using temporary db and postprocessing
                return genres, "CREATE TABLE shows (id text, title text, desc text, " + " INTEGER DEFAULT 0, ".join(
                    ids) + " INTEGER DEFAULT 0, prodstart INTEGER, prodend INTEGER, actors text, producers text, regisseurs text, authors text, langs text);"
            genres = []
            lis_ = []
            elem_genres = BeautifulSoup(requests.get("https://bs.to/andere-serien").text, features="html.parser").find_all("div", {"class": "genre"})
            for i, element_ in enumerate(elem_genres):
                self.emit(self.sig_genre, i, len(elem_genres))
                elem_shows = element_.find_all("li")
                for j, show_ in enumerate(elem_shows):
                    self.emit(self.sig_show, j, len(elem_shows))
                    while len(lis_) > 50:
                        time.sleep(0.1)
                    with generator_primary_lock:
                        lis_.append(GenreFetcher(urljoin("https://bs.to/andere-serien", show_.find("a")["href"])))
                        lis_[len(lis_) - 1].start()
            thr_max = len(lis_)
            while len(lis_) > 0:
                time.sleep(0.1)
                # TODO Remove debug statement
                open("../gr.txt", "w+").write(str(len(lis_)))
                self.emit_toplevel(1, len(lis_), thr_max)
            self.emit_toplevel(2)
            for result in generator_results:
                genres = list(set(genres) | set(result))
            ids = []
            genres.sort()
            for genre in genres:
                ids.append("genre_" + genre.lower().replace("-", "_").replace(" ", "_"))
            # TODO switch language for optimized approach (like with genres)
            # TODO get rid of Tag-Loader (generator) by using temporary db and postprocessing
            return genres, "CREATE TABLE shows (id text, title text, desc text, " + " INTEGER DEFAULT 0, ".join(
                ids) + " INTEGER DEFAULT 0, prodstart INTEGER, prodend INTEGER, actors text, producers text, regisseurs text, authors text, langs text);"

        def add_show(id, title, desc, genres, prod_start, prod_end, actors, producers, regisseurs, authors, langs):
            temp = []
            for genre in genres:
                if genre in all_genres:
                    temp.append("genre_" + genre.lower().replace("-", "_").replace(" ", "_"))
            ids = ", ".join(temp)
            ones = ", ".join(["1" for xui in genres])
            actors = ";" + ";".join(actors).replace("'", "''") + ";"
            producers = ";" + ";".join(producers).replace("'", "''") + ";"
            regisseurs = ";" + ";".join(regisseurs).replace("'", "''") + ";"
            authors = ";" + ";".join(authors).replace("'", "''") + ";"
            langs = ";" + ";".join(langs) + ";"
            with sqlite_lock:
                # TODO Remove debug statement
                sql = f"INSERT INTO shows(id, title, desc, {ids}, prodstart, prodend, actors, producers, regisseurs, authors, langs) VALUES ('{id}', '{title}', '{desc}', {ones}, {prod_start}, {prod_end}, '{actors}', '{producers}', '{regisseurs}', '{authors}', '{langs}')"
                cur.execute(f"INSERT INTO shows(id, title, desc, {ids}, prodstart, prodend, actors, producers, regisseurs, authors, langs) VALUES ('{id}', ?, ?, {ones}, ?, ?, ?, ?, ?, ?, ?)", [title, desc, prod_start, prod_end, actors, producers, regisseurs, authors, langs])

        class Fetcher(threading.Thread):
            def __init__(self, _url):
                super().__init__()
                self.url = _url

            def run(self) -> None:
                tags = []
                html = BeautifulSoup(requests.get(self.url).text, features="html.parser")
                attribute_table = html.find("div", {"class": "infos"}).find_all("div")
                for elem in attribute_table[0].find("p").find_all("span"):
                    tags.append(elem.text)
                production_year_start, production_year_end = str(attribute_table[1].find("em").text).split(" - ")
                production_year_start = int(0 if production_year_start == "Unbekannt" else production_year_start)
                production_year_end = int(datetime.datetime.now().year if production_year_end == "Unbekannt" else production_year_end)
                main_actors = []
                for elem in attribute_table[2].find("p").find_all("span"):
                    main_actors.append(elem.text)
                producers = []
                for elem in attribute_table[3].find("p").find_all("span"):
                    producers.append(elem.text)
                regisseurs = []
                for elem in attribute_table[4].find("p").find_all("span"):
                    regisseurs.append(elem.text)
                authors = []
                for elem in attribute_table[5].find("p").find_all("span"):
                    authors.append(elem.text)
                langs = []
                for ej in html.select("#seasons a"):
                    htx = BeautifulSoup(requests.get("https://bs.to/" + ej['href']).text, features="html.parser")
                    try:
                        for elem in htx.select("div.series-language")[0].find_all("li"):
                            if elem["data-value"] not in langs:
                                langs.append(elem["data-value"])
                    except IndexError:
                        pass
                    num1_ = 0 if ej.text == "Specials" else int(ej.text)
                    for el in htx.find("table", {"class": "episodes"}).find_all("tr"):
                        title_ = el.find("td").find("a")['title']
                        num2_ = int(el.find("td").find("a").text)
                        html_ = BeautifulSoup(requests.get("https://bs.to/" + el.find("td").find("a")['href']).text, features="html.parser")
                        desc_ = html_.find("p", {"id": "spoiler"}).text
                        with sqlite_lock:
                            cur.execute(f"INSERT INTO episodes(show_id, season_number, episode_number, title, desc) VALUES (?, ?, ?, ?, ?)", [self.url, num1_, num2_, title_, desc_])
                title = html.find('h2').text.replace('\t', '').strip("\t\n ").split("\n")[0]
                desc = html.find("div", {"id": "sp_left"}).find("p").text
                add_show(self.url, title.replace("'", "''"), desc.replace("'", "''"), tags, production_year_start, production_year_end, main_actors, producers, regisseurs, authors, langs)
                lis.remove(self)
                with sqlite_lock:
                    con.commit()

        try:
            os.remove("../bs.db")
            os.remove("../bs.db-journal")
        except:
            pass
        sqlite_lock = Lock()
        genre_lock = Lock()
        if isfile("../xgenres.json"):
            os.remove("../genres.json")
        all_genres, shows_sql = generator()
        json.dump(all_genres, open("../genres.json", "w+"), indent=4)
        con = sqlite3.connect('../bs.db', check_same_thread=False)
        cur = con.cursor()
        cur.execute(shows_sql)
        cur.execute("CREATE TABLE episodes (show_id text, season_number INTEGER, episode_number INTEGER, title text, desc text)")
        lis = []
        self.emit_toplevel(3)
        elem_genres = BeautifulSoup(requests.get("https://bs.to/andere-serien").text, features="html.parser").find_all("div", {"class": "genre"})
        for i, element in enumerate(elem_genres):
            self.emit(self.sig_genre, i, len(elem_genres))
            elem_shows = element.find_all("li")
            for j, show in enumerate(elem_shows):
                self.emit(self.sig_show, j, len(elem_shows))
                while len(lis) > 100:
                    time.sleep(0.1)
                lis.append(Fetcher(urljoin("https://bs.to/andere-serien", show.find("a")["href"])))
                lis[len(lis) - 1].start()
        thr_max = len(lis)
        while not input("ENTER").startswith("y"):
            time.sleep(0.1)
            print(len(lis))
            self.emit_toplevel(1, len(lis), thr_max)
        con.commit()
        sys.stdout.flush()
        os.execv(sys.executable, ['python'] + [sys.argv[0]])


def refresh():
    application.refresh_agent = Refresher()


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(ApplicationWindow, self).__init__()
        self.refresh_agent = None
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)


def searchSubmit():
    con = sqlite3.connect('../bs.db')
    cur = con.cursor()
    cur2 = con.cursor()
    table = []
    for row in cur.execute(parser.parse(ui.textSearch.text())):
        table.append(cur2.execute("SELECT title, desc, id FROM shows WHERE id=? ORDER BY title", [row[0]]).fetchone())
    cur.close()
    cur2.close()
    con.close()
    del cur
    del cur2
    del con
    ui.populate(table)


if __name__ == "__main__":
    import sys
    parser = searchTermParser()
    app = QtWidgets.QApplication(sys.argv)
    application = ApplicationWindow()
    application.setStyleSheet(qdarkgraystyle.load_stylesheet())
    ui = application.ui
    ui.buttonRefresh.clicked.connect(refresh)
    ui.textSearch.returnPressed.connect(searchSubmit)
    application.show()
    if isfile("../genres.json") and isfile("../bs.db"):
        all_genres = json.load(open("../genres.json", "r"))
    else:
        application.refresh_agent = Refresher()
    sys.exit(app.exec_())
