from urllib3.util import connection
import dns.resolver



class modDNS:
    _dns_providers = {
        "cloudflare": [
            "1.1.1.1",
            "1.0.0.1",
        ],
        "google": [
            "8.8.8.8",
            "8.8.4.4", ],
        "censurfridns": [
            "89.233.43.71",
            "130.225.244.166",
            "130.226.161.34",
            "185.38.27.139",
            "198.180.150.12",
        ],
        "dns.watch": [
            "84.200.69.80",
            "84.200.70.40",
        ],
        "opendns": [
            "208.67.222.222",
            "208.67.220.220",
            "208.67.222.220",
            "208.67.220.222",
        ],
        "opennic": [
            "194.36.144.87",
        ],
        "freedns": [
            "37.235.1.177",
            "172.104.237.57",
            "172.104.49.100",
            "45.33.97.5",
        ],
        "yandex.dns": [
            "77.88.8.8",
            "77.88.8.1",
        ],
        "safeserve": [
            "198.54.117.10",
            "198.54.117.11",
        ],
        "safe_dns": [
            "195.46.39.39",
            "195.46.39.40",
        ],
    }
    _orig_create_connection = connection.create_connection

    def __init__(self, provider: str, timeout: float = 5.0):
        self._provider = self._dns_providers[provider] if provider in self._dns_providers else [provider]
        self._timeout = timeout

    def myResolver(self, _host, dnssrv):
        r = dns.resolver.Resolver()
        r.nameservers = dnssrv
        r.lifetime = self._timeout
        answers = r.resolve(_host)
        for rdata in answers:
            return str(rdata)

    def patched_create_connection(address, *args, **kwargs):
        """Wrap urllib3's create_connection to resolve the name elsewhere"""
        # resolve hostname to an ip address; use your own
        # resolver here, as otherwise the system resolver will be used.
        _host, _port = address
        _hostname = self.myResolver(_host, self._provider)

        return self._orig_create_connection((_hostname, _port), *args, **kwargs)

    connection.create_connection = patched_create_connection
