import json


class searchTermParser:
    def parse(self, s: str) -> str:
        parsedPrimary, returnCode = self._parenthesesParser(s)
        if parsedPrimary is not None:
            parsedSecondary = self._booleanParser(parsedPrimary)
        else:
            raise SyntaxError(returnCode)
        return f"SELECT DISTINCT S.id FROM shows S INNER JOIN episodes E on S.id = E.show_id WHERE {parsedSecondary} ORDER BY S.title, E.season_number, E.episode_number"

    def _parenthesesParser(self, st: str, top: bool = True):
        ret = [""]
        isNext = False
        jump = 0
        ju = 0
        i = 0
        quotes = False
        for i in range(0, len(st)):
            ju += 1
            if jump > 0:
                jump -= 1
                continue
            elif st[i] == "(" and (st[i - 1] != "\\" or st[i - 2:i] == "\\\\"):
                isNext = True
                lis, jump = self._parenthesesParser(st[i + 1:], False)
                if lis is None:
                    return None, i + jump + 1
                ret.append(lis)
            elif st[i] == ")" and (st[i - 1] != "\\" or st[i - 2:i] == "\\\\"):
                if top:
                    return None, i
                else:
                    return ret, ju
            elif st[i] == " " and not quotes:
                isNext = True
            elif st[i] == "\"" and (st[i - 1] != "\\" or st[i - 2:i] == "\\\\"):
                quotes = not quotes
            elif st[i] == "\\" and st[i - 1] != "\\":
                continue
            else:
                if isNext:
                    ret.append(st[i])
                    isNext = False
                else:
                    ret[len(ret) - 1] = ret[len(ret) - 1] + st[i]
        if top:
            return ret, 0
        else:
            return None, i

    def _conditionalParser(self, st: str, dot1) -> str:
        not_ = False
        if st.startswith("!"):
            st = st[1:]
            not_ = True
        if st.startswith("desc:"):
            return f"S.desc {'NOT ' if not_ else ''}LIKE '%{st[5:]}%'"
        elif st.startswith("title:"):
            return f"S.title {'NOT ' if not_ else ''}LIKE '%{st[6:]}%'"
        elif st.startswith("tag:"):
            if st[4:].lower() in list((map(lambda x: x.lower(), json.load(open("../genres.json", "r"))))):
                return f"S.genre_{st[4:].lower().replace('-', '_').replace(' ', '_')}{'!' if not_ else ''}=1"
            return "1=" + ("1" if dot1 else "0")
        else:
            return f"(S.desc {'NOT ' if not_ else ''}LIKE '%{st}%' OR S.title {'NOT ' if not_ else ''}LIKE '%{st}%' OR E.desc {'NOT ' if not_ else ''}LIKE '%{st}%' OR E.title {'NOT ' if not_ else ''}LIKE '%{st}%')"

    def _booleanParser(self, li: list, dot1=None) -> str:
        if dot1 is None:
            dot1 = li.__contains__(".:1")
        if li.__contains__(".:1"):
            li.remove(".:1")
        ret = "("
        for i in range(0, len(li)):
            if isinstance(li[i], list):
                ret = ret + (((" OR " if li[i - 1] == "OR" else " AND ") if i > 0 else "") + self._booleanParser(li[i]))
            elif li[i] == "OR":
                continue
            else:
                ret = ret + (((" OR " if li[i - 1] == "OR" else " AND ") if i > 0 else "") + self._conditionalParser(li[i], dot1))
        return ret + ")"
